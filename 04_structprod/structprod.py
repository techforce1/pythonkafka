"""
structprod.py: produceer een kafka topic gestructureerde stream met formaat:


 scr_ip: string representatie van IP adres (source)
 src_port: int met source port nummer van connectie
 dst_ip: string representatie van IP adres (destination)
 dst_port: int met destination port nummer van connectie

"""

import faust

class IPFlow(faust.Record):
    src_ip: str
    src_port: int
    # vul hier het model aan voor de (de)serializer

app = faust.App('structprod', broker='kafka-1:19092')
topic = app.topic('ipflow', value_type=IPFlow)


@app.timer(interval=1.0)
async def ipflow_sender(app):
    await topic.send(
        value=IPFlow(src_ip='10.10.10.10', src_port=31000,dst_ip='192.168.10.10',dst_port=80)
    )

if __name__ == '__main__':
    app.main()