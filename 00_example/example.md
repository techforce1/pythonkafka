## Simpel voorbeeld Producer / Consumer

Om te beginnen twee simpele Faust agents:
- exampleproducer: schrijft string naar topic "example", opstarten vanuit terminal window (interactief voor info/debug output):

$ python3 ./exampleproducer.py worker -l info --without-web

- examplecosnumer: leest topic "example", opstarten vanuit terminal window (interactief voor info/debug output):

$ python3 ./exampleconsumer.py worker -l info --without-web
