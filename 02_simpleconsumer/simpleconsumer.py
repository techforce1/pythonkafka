"""
Maak op basis van de example simple consumer (00_example) een simpele consumer die:
- alle berichten consumeert van topic Craftsmen en weergeeft naar stdout (terminal)
"""
import faust

app = faust.App('simpleconsumer', broker='kafka-1:19092')
topic = app.topic(#vul aan)

@app.agent(topic)
async def process(stream):
    async for value in stream:
        ### actie met geconsumeerde berichten toevoegen

if __name__=="__main__":
    app.main()